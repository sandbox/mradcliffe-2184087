## About

AtTask is a Project-Portfolio Management (PPM) software as a service. In human terms, this means it is a top-down project management tool.

This module provides integration with AtTask's Restful API specifically for the purpose of adding issue collection for a Drupal web site.

## Dependencies

- [Entity API](http://drupal.org/project/entity): AtTask Projects are entities.
- [CTools](http://drupal.org/project/ctools): The issue collector and project entity form are ctools ajax/wizard forms.

## Configuration

1. Go to ```admin/configure/services/attask```, and configure the AtTask domain for your project.
   - Note that only attask on-demand is supported at the moment.
   - You may also configure the position and alignment of the issue collector modal link.
2. Add a project at ```admin/configure/services/attask/project/add```
   1. The first step of adding or modifying a project is to login with your AtTask user name and password. This does a pass-through via the Form API instead of a purely client-side implementation (see Roadmap).
   2. Then choose a project from the available AtTask projects in your domain, and provide a list of Drupal paths that it will relate to on your site.
      - Examples: ```<front>``` ```node/*``` ```user/*```

## Roadmap

- [Guzzle](http://github.com/guzzle/guzzle) and/or standalone JS library to make requests client-side.
